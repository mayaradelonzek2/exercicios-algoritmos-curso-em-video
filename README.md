Projeto desenvolvido durante o curso de Algoritmo do **Curso em Vídeo** (https://www.cursoemvideo.com/course/curso-de-algoritmo/).

**Nos exercícios foram aplicados os seguintes conceitos:**
* Operadores lógicos e relacionais;
* Estruturas condicionais;
* Estruturas de repetição;
* Procedimentos;
* Funções;
* Vetores;
* Matrizes.

**Ferramentas utilizadas:**
* VisuAlg Versão 2.0;
* Windows 10 Versão 1909 (OS Build 18363.1379);

**Para visualizar as funcionalidades dos exercícios realizados durante as aulas, abra cada arquivo no VisuAlg e execute-os.**
